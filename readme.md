# 仓库简介<a name="ZH-CN_TOPIC_0000001387319030"></a>

SaaS Landing开源项目以帮助ISV基于华为云构建SaaS应用为目的，是SaaS应用中公共模块的一部分，是供伙伴参考使用的示例代码，使能SaaS合作伙伴基于华为云自助式完成SaaS应用构建。SaaS Landing作为SaaS和华为云之间的粘合剂，能够进一步繁荣华为云生态，为华为云带来价值。

# 项目总览<a name="ZH-CN_TOPIC_0000001387479658"></a>

<table><thead align="left"><tr id="row1443311132510"><th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.2.4.1.1"><p id="p131914416813"><a name="p131914416813"></a><a name="p131914416813"></a>项目</p>
</th>
<th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.2.4.1.2"><p id="p53193441181"><a name="p53193441181"></a><a name="p53193441181"></a>简介</p>
</th>
<th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.2.4.1.3"><p id="p1319944383"><a name="p1319944383"></a><a name="p1319944383"></a>仓库</p>
</th>
</tr>
</thead>
<tbody><tr id="row10433116255"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.2.4.1.1 "><p id="p124339117255"><a name="p124339117255"></a><a name="p124339117255"></a>SaaS应用构建</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.2.4.1.2 "><p id="p74334142511"><a name="p74334142511"></a><a name="p74334142511"></a>该仓库开源版本包含3个能力中心及公共模块的示例代码</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.2.4.1.3 "><p id="p1114165532614"><a name="p1114165532614"></a><a name="p1114165532614"></a><strong id="b8114105513261"><a name="b8114105513261"></a><a name="b8114105513261"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-saaslanding-sample" target="_blank" rel="noopener noreferrer">huaweicloud-saas-landing-sample</a></strong></p>
</td>
</tr>
</tbody>
</table>

